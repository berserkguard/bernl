#pragma once

 /**
 * @author Ryan Norby
 * @version 1.0
 *
 * @brief A listing of all the possible types of packets, for both client & server.
 */
enum PacketType {
    PACKET_INIT,		///< Initial packet sent by client, used for connecting to the server.
    PACKET_RESPONSE,	///< Initial packet sent to the client, used for verifying connection.
    PACKET_MESSAGE		///< Contains a username & message, used for transferring chat messages.
};
