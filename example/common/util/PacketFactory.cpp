#include "PacketFactory.h"

Packet* PacketFactory::getPacket(char* data){
	switch(*reinterpret_cast<PacketType*>(data)){
	case PACKET_INIT:
		return new InitPacket(data);
	case PACKET_RESPONSE:
		return new ResponsePacket(data);
	case PACKET_MESSAGE:
		return new MessagePacket(data);
	}
	return (Packet*)0;
}
