#pragma once

#include "packets.h"

/**
 * @author Ryan Norby
 * @version 1.0
 *
 * @brief Utility class for deserializing a packet without explicitly stating its type.
 */
class PacketFactory {
public:
	/**
	 * Returns a pointer to an allocated Packet representing the serialized data.
	 * @param data A serialized packet.
	 * @return A pointer to a valid Packet, or NULL.
	 */
	static Packet* getPacket(char* data);
};
