#pragma once

/**
 * @file
 * @author Ryan Norby
 * @version 1.0
 *
 * @brief Includes every custom Packet header.
 */

#include "packets/Packet.h"

#include "PacketType.h"

#include "packets/InitPacket.h"
#include "packets/ResponsePacket.h"
#include "packets/MessagePacket.h"
