#pragma once

#include "packets/Packet.h"
#include <string>

 /**
 * @author Ryan Norby
 * @version 1.0
 *
 * @brief The initial packet returned to a client from the server.
 *
 * Includes a custom message which can be interpreted by the client, for example to
 * distinguish between success and failure.
 */
class MessagePacket : public Packet {
public:
	/** Initializes packet to have type PACKET_MESSAGE and message "". */
	MessagePacket();

	/** Initializes packet to match a serialized version of the packet. */
	MessagePacket(char* serializedData);

	/** Frees any memory used by the packet. */
	~MessagePacket();

	/**
	 * Sets the message to be returned to the client.
	 * @param message The string to send to the client. For example, could be "Connection successful!"
	 */
	void setMessage(std::string message);

	/**
	 * Returns the message contained in the packet.
	 * @return The message returned by the server.
	 */
	std::string getMessage();

	/**
	 * Sets the message to be returned to the client.
	 * @param message The string to send to the client. For example, could be "Connection successful!"
	 */
	void setUsername(std::string username);

	/**
	 * Returns the message contained in the packet.
	 * @return The message returned by the server.
	 */
	std::string getUsername();

	// Overridden methods. See Packet.h for documentation.
	char* serialize();
	void deserialize(char* data);
	unsigned int getSize();

private:
	std::string message; ///< A message to be displayed to the client.
	std::string username;
};

