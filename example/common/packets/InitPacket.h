#pragma once

#include "packets/Packet.h"
#include <string>

/**
 * @author Ryan Norby
 * @version 1.0
 *
 * @brief Initial packet sent from the client to the server. Requests a connection.
 */
class InitPacket : public Packet {
public:
	/** Initializes packet to have type PACKET_INIT and username NULL. */
	InitPacket();

	/** Initializes packet to match a serialized version of the packet. */
	InitPacket(char* serializedData);

	/** Frees all memory used by the packet. */
	~InitPacket();

	/**
	 * Sets the username to be sent to the server.
	 * @param username The string to send to the server. For example, could be "Player1"
	 */
	void setUsername(std::string username);

	/**
	 * Returns the username contained in the packet.
	 * @return The username sent by the client.
	 */
	std::string getUsername();

	// Overridden methods. See Packet.h for documentation.
	char* serialize();
	void deserialize(char* data);
	unsigned int getSize();

private:
	std::string username; ///< The username for the client to connect to the server under. Should be unique.
};
