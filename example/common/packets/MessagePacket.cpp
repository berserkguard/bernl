#include "MessagePacket.h"
#include "PacketType.h"
#include <iostream>

MessagePacket::MessagePacket() : Packet(PACKET_MESSAGE), message(""), username("") {}

MessagePacket::MessagePacket(char* serializedData) : MessagePacket() {
	deserialize(serializedData);
}

MessagePacket::~MessagePacket(){}

char* MessagePacket::serialize(){
	Serializer s;
	s.push(type);
	s.push(username);
	s.push(message);

	return s.getPayload();
}

void MessagePacket::deserialize(char* data){
	Deserializer d(data);

	if(d.pop<PacketType>() != type){
		std::cout << "Error: packet type mismatch." << std::endl;
		return;
	}
	username = d.pop<std::string>();
	message = d.pop<std::string>();
}

unsigned int MessagePacket::getSize(){
	return sizeof(type) + (message.length() + 1) + (username.length() + 1);
}

void MessagePacket::setMessage(std::string message){
	this->message = std::string(message);
}

std::string MessagePacket::getMessage(){
	return message;
}

void MessagePacket::setUsername(std::string username){
	this->username = std::string(username);
}

std::string MessagePacket::getUsername(){
	return username;
}
