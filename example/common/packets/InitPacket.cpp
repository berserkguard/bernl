#include "InitPacket.h"
#include "PacketType.h"
#include <iostream>

InitPacket::InitPacket() : Packet(PACKET_INIT), username("") {}

InitPacket::InitPacket(char* serializedData) : InitPacket() {
	deserialize(serializedData);
}

InitPacket::~InitPacket(){}

char* InitPacket::serialize(){
	Serializer s;
	s.push(type);
	s.push(username);

	return s.getPayload();
}

void InitPacket::deserialize(char* data){
	Deserializer d(data);

	if(d.pop<PacketType>() != type){
		std::cout << "Error: packet type mismatch." << std::endl;
		return;
	}
	username = d.pop<std::string>();
}

unsigned int InitPacket::getSize(){
	return sizeof(type) + (username.length() + 1);
}

void InitPacket::setUsername(std::string username){
	this->username = std::string(username);
}

std::string InitPacket::getUsername(){
	return username;
}
