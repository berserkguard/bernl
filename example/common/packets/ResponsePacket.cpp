#include "ResponsePacket.h"
#include "PacketType.h"
#include <iostream>

ResponsePacket::ResponsePacket() : Packet(PACKET_RESPONSE), message("") {}

ResponsePacket::ResponsePacket(char* serializedData) : ResponsePacket() {
	deserialize(serializedData);
}

ResponsePacket::~ResponsePacket(){};

char* ResponsePacket::serialize(){
	Serializer s;
	s.push(type);
	s.push(message);

	return s.getPayload();
}

void ResponsePacket::deserialize(char* data){
	Deserializer d(data);

	if(d.pop<PacketType>() != type){
		std::cout << "Error: packet type mismatch." << std::endl;
		return;
	}
	message = d.pop<std::string>();
}

unsigned int ResponsePacket::getSize(){
	return sizeof(type) + (message.length() + 1);
}

void ResponsePacket::setMessage(std::string message){
	this->message = std::string(message);
}

std::string ResponsePacket::getMessage(){
	return message;
}
