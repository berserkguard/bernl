#include "ServerGame.h"
#include <iostream>
#include <csignal>

// Let program clean up before terminating
namespace {
	volatile sig_atomic_t running = 1;

	void signalHandler(int sig){
		signal(sig, signalHandler);
		running = 0;
	}
}

int main(){
	// Register signal interrupts
	signal(SIGINT, signalHandler);
    signal(SIGTERM, signalHandler);
#ifdef SIGBREAK
    signal(SIGBREAK, signalHandler);
#endif

    std::cout << "Starting server. Press Ctrl+C to terminate." << std::endl;

    ServerGame server;
    while(running){
		// Handle server logic
		server.update();
    }
    return 0;
}
