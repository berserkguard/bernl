#include "ServerGame.h"
#include "util/PacketFactory.h"
#include <iostream>

ServerGame::ServerGame(){
	// Listen for incoming connections on port 6881
	network = new ServerNetwork(6881);
	network->registerPacketDecoder(PacketFactory::getPacket);
}
ServerGame::~ServerGame(){
	delete network;
}

void ServerGame::update(){
	// Check if any clients are waiting to connect
	if(unsigned int client = network->acceptNewClient()){
		std::cout << "Client connected (id: " << client << ")" << std::endl;
		std::cout << "    Num connections: " << network->getNumConnections() << std::endl;
	}

	// Process any packets received from client
	receiveFromClients();
}

void ServerGame::receiveFromClients(){
	// Iterate over every connected client
	while(int curClient = network->fetchClient()){
		// Process every packet queued up for the current client
		while(Packet* packet = network->fetchPacket(curClient)){
			switch(packet->getType()){
			case PACKET_INIT:
				std::cout << "Received InitPacket from client." << std::endl;
				std::cout << "    username: " << ((InitPacket*)packet)->getUsername() << std::endl;
				{
					ResponsePacket rp;
					rp.setMessage("Welcome to our server!");
					network->dispatch(curClient, rp);
				}
				break;
			case PACKET_MESSAGE:
				{
					MessagePacket* mp = (MessagePacket*)packet;
					std::cout << "Received MessagePacket from client." << std::endl;
					std::cout << "  " << mp->getUsername() << ": " << mp->getMessage() << std::endl;
					network->broadcast(*mp);
				}
				break;
			default:
				std::cout << "Warning: Unhandled packet type: " << (int)packet->getType() << std::endl;
				break;
			}
			delete packet;
		}
	}
}
