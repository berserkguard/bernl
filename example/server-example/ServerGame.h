#pragma once

#include "ServerNetwork.h"

/**
 * @author Ryan Norby
 * @version 1.0
 *
 * @brief Manages the logic for a server and handles interaction with clients.
 */
class ServerGame {
public:
	/** Initializes the server network layer. */
	ServerGame();

	/** Frees all memory used by the server. */
	~ServerGame();

	/** Polls for new connections and processes all packets received. */
    void update();

private:
	/** Helper function that processes all packets received. */
	void receiveFromClients();

	ServerNetwork* network; ///< The network layer for the server.
};
