#pragma once

#include "ClientNetwork.h"

/**
 * @author Ryan Norby
 * @version 1.0
 *
 * @brief Manages the logic for a client and handles interaction with the server.
 */
class ClientGame {
public:
	/** Initializes the client network layer and attempts to connect to a server. */
	ClientGame();

	/** Frees all memory used by the client. */
	~ClientGame();

	/** Polls server for data, and processes any packets retrieved. */
    void update();

private:
	ClientNetwork* network; ///< The network layer for the client.
};

