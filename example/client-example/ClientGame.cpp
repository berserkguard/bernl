#include "ClientGame.h"
#include <iostream>
#include "util/PacketFactory.h"
#include <process.h>

HANDLE threadHnd;
char name[50];

unsigned int __stdcall pollFunc(void* data){
	ClientNetwork* network = (ClientNetwork*)data;
	char input[256];

	while(true){
		//std::cout << name << ": ";
		std::cin.getline(input, 256);

		if(input[0] != '\0'){
			MessagePacket packet;
			packet.setUsername(name);
			packet.setMessage(input);
			network->dispatch(packet);
		}
	}
	return 0;
}

ClientGame::ClientGame(){
	// Connect to the server at IP 127.0.0.1 over port 6881
	network = new ClientNetwork("127.0.0.1", 6881);
	network->registerPacketDecoder(PacketFactory::getPacket);

	// Let the user pick a name
	std::cout << "Enter a name: ";
	std::cin.getline(name, 50);

	// Send the initial packet to the server
	InitPacket packet;
	packet.setUsername(name);
	network->dispatch(packet);
}

ClientGame::~ClientGame(){
	TerminateThread(threadHnd, 0);
	delete network;
}

void ClientGame::update(){
	// Process every packet in the queue
	while(Packet* packet = network->fetchPacket()){
		switch(packet->getType()){
		case PACKET_RESPONSE:
			{
				ResponsePacket* rp = (ResponsePacket*)packet;
				std::cout << "Successfully connected to server." << std::endl;
				std::cout << "    MOTD: " << rp->getMessage() << std::endl;
				threadHnd = (HANDLE)_beginthreadex(0, 0, pollFunc, (void*)network, 0, NULL);
			}
			break;
		case PACKET_MESSAGE:
			{
				MessagePacket* mp = (MessagePacket*)packet;
				if(mp->getUsername().compare(name) != 0){
					std::cout << mp->getUsername() << ": " << mp->getMessage() << std::endl;
				}
			}
			break;
		default:
			std::cout << "Warning: Unhandled packet type: " << (int)packet->getType() << std::endl;
			break;
		}
		delete packet;
	}
}
