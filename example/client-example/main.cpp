#include "ClientGame.h"
#include <iostream>
#include <csignal>
#include <ctime>

// Let program clean up before terminating
namespace {
	volatile sig_atomic_t running = 1;

	void signalHandler(int sig){
		signal(sig, signalHandler);
		running = 0;
	}
}

int main(){
	// Register signal interrupts
	signal(SIGINT, signalHandler);
    signal(SIGTERM, signalHandler);
#ifdef SIGBREAK
    signal(SIGBREAK, signalHandler);
#endif

	std::cout << "Starting client..." << std::endl;

	ClientGame client;
	while(running){
		clock_t start = clock();

		// Handle client logic
		client.update();

		// Update no more than 60 times per second
		float diff = (clock() - start) / (float)CLOCKS_PER_SEC;
		if(diff < 1.0f / 60.0f){
			Sleep(1000.0f * (1.0f / 60.0f - diff));
		}
	}
	return 0;
}
