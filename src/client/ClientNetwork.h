#pragma once

// Network includes
#define _WIN32_WINNT 0x501
#include <winsock2.h>
#include <ws2tcpip.h>

#include "packets/Packet.h"

/**
 * @author Ryan Norby
 * @version 1.0
 *
 * @brief Handles the transfer of data between the client and the server over a socket.
 */
class ClientNetwork {
public:
	/**
	 * Attempts to connect to the given address on the given port.
	 * @param address The IPv4 or IPv6 address of the server to connect to.
	 * @param port The port number of the server to connect over.
	 */
	ClientNetwork(const char* address, int port);

    /** Frees any memory used by the ClientNetwork. */
	~ClientNetwork();

	/** Disconnects from the server. */
	void disconnect();

	/**
	 * Pops off a Packet from the top of the queue. Returns NULL when no more Packets need to be processed.
	 * @note This function should be called in the predicate of a while loop in order to process all packets.
	 * @return A valid Packet from the top of the queue, or NULL if all Packets have been processed.
	 */
	Packet* fetchPacket();

	/**
	 * Sends the specified packet to the server.
	 * @param packet The packet to send to the server.
	 * @return The number of bytes sent, or SOCKET_ERROR if failure.
	 */
	int dispatch(Packet& packet);

	/**
	 * Sets which function to use for decoding raw packets. This is a necessary 'hack' in order for the API to
	 * not have to know about custom packet types. The supplied function should implement a Factory pattern that
	 * constructs a new Packet of the appropriate type (and deserializes!).
	 * @param func A pointer to a function to use for decoding custom packet types.
	 */
	void registerPacketDecoder(Packet* (*func)(char*));

private:
	SOCKET connectSocket; ///< Socket to connect to server on.
	char networkBuffer[Packet::MAX_SIZE]; ///< Used for reading data from the socket.
	int totalBytes; ///< Stores the total bytes read into networkBuffer.
	int bytesRead; ///< Keeps track of the number of bytes processed in networkBuffer.
	Packet* (*decodeFunc)(char*); ///< A pointer to the function to use for decoding packets.
};

