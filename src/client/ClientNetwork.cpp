#include "ClientNetwork.h"
#include <iostream>
#include <sstream>

ClientNetwork::ClientNetwork(const char* address, int port){
	connectSocket = INVALID_SOCKET;
	bytesRead = -1;
	decodeFunc = NULL;

	// Initialize Winsock
	WSADATA wsaData;
	if(WSAStartup(MAKEWORD(2, 2), &wsaData)){
		std::cout << "WSAStartup failed. Exiting." << std::endl;
		exit(1);
	}

	// Address info for setting up the client socket
	addrinfo hints, *result = NULL, *ptr = NULL;

	// Configure hints for requesting TCP socket
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	std::stringstream ss;
	ss << port;
	if(getaddrinfo(address, ss.str().c_str(), &hints, &result)){
		std::cout << "getaddrinfo failed (" << WSAGetLastError() << "). Exiting." << std::endl;
		WSACleanup();
		exit(1);
	}

	// Keep trying to connect to addresses until one succeeds
	for(ptr = result; ptr; ptr = ptr->ai_next){
		// Create a SOCKET for connecting to server
		connectSocket = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if(connectSocket == INVALID_SOCKET){
			std::cout << "socket failed (" << WSAGetLastError() << "). Exiting." << std::endl;
			WSACleanup();
			exit(1);
		}

		// Connect to the server
		if(connect(connectSocket, ptr->ai_addr, (int)ptr->ai_addrlen) == SOCKET_ERROR){
			closesocket(connectSocket);
			connectSocket = INVALID_SOCKET;
			std::cout << "connect failed. Is the server running?" << std::endl;
			freeaddrinfo(result);
			WSACleanup();
			exit(1);
		}
	}
	freeaddrinfo(result);

	// Set the mode of the socket to be nonblocking
	unsigned long iMode = 1;
	if(ioctlsocket(connectSocket, FIONBIO, &iMode) == SOCKET_ERROR){
		std::cout << "ioctlsocket failed (" << WSAGetLastError() << "). Exiting." << std::endl;
		closesocket(connectSocket);
		WSACleanup();
		exit(1);
	}

	// Disable Nagle to speed up TCP delivery
	char value = 1;
	setsockopt(connectSocket, IPPROTO_TCP, TCP_NODELAY, &value, sizeof(value));
}

ClientNetwork::~ClientNetwork(){
	disconnect();
	WSACleanup();
}

void ClientNetwork::disconnect(){
	closesocket(connectSocket);
	std::cout << "Disconnected from server." << std::endl;
}

void ClientNetwork::registerPacketDecoder(Packet* (*func)(char*)){
	decodeFunc = func;
}

Packet* ClientNetwork::fetchPacket(){
	if(bytesRead == -1){
		totalBytes = recv(connectSocket, networkBuffer, Packet::MAX_SIZE, 0);
		if(totalBytes == 0){
			disconnect();
			return NULL;
		}else if(totalBytes < 0){
			return NULL; // Error occurred
		}
		bytesRead = 0;
	}

	if(bytesRead >= totalBytes){
		// Processed all packets in queue, reset
		bytesRead = -1;
		return NULL;
	}

	if(!decodeFunc){
		// A decoding function was never supplied!
		std::cout << "Error, no custom decoding function supplied. See registerPacketDecoder()." << std::endl;
		return NULL;
	}
	Packet* packet = decodeFunc(&networkBuffer[bytesRead]);
	if(!packet){
		std::cout << "Invalid packet received." << std::endl;
		bytesRead = -1;
		return NULL;
	}
	bytesRead += packet->getSize();

	return packet;
}

int ClientNetwork::dispatch(Packet& packet){
	char* data = packet.serialize();
	int result = send(connectSocket, data, packet.getSize(), 0);
	delete[] data;
	return result;
}
