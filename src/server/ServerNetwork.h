#pragma once

// Network includes
#define _WIN32_WINNT 0x501
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>

#include <map>
#include "packets/Packet.h"

/**
 * @author Ryan Norby
 * @version 1.0
 *
 * @brief Handles the transfer of data over sockets open between the server and clients.
 */
class ServerNetwork {
public:
	/**
	 * Configures the socket and begins listening for client connections.
	 * @param port The port number to listen on.
	 * @param maxConnections The maximum number of concurrent connections to allow (default: 8).
	 */
	ServerNetwork(int port, int maxConnections = 8);

	/** Frees any memory used by the ServerNetwork. */
	~ServerNetwork();

	/**
	 * Dispatches a single packet to all connected clients.
	 * @param packet The packet to send.
	 */
	void broadcast(Packet& packet);

	/**
	 * Sends the specified packet to the specified client.
	 * @param clientID The client to send the packet to.
	 * @param packet The packet to send to the client.
	 * @return The number of bytes sent, or SOCKET_ERROR if failure.
	 */
	int dispatch(unsigned int clientID, Packet& packet);

	/**
	 * Pops off a Packet from the top of the queue for the given client. Returns NULL when no more Packets need to be processed.
	 * @note This function should be called in the predicate of a while loop in order to process all packets for a client.
	 *       It is also important to note that multiple calls for each client ID must be consecutive, otherwise data will be lost.
	 * @param clientID The client whose packet to pop off the queue.
	 * @return A valid Packet from the top of the queue, or NULL if all Packets have been processed for the client.
	 */
	Packet* fetchPacket(unsigned int clientID);

	/**
	 * Used to iterate over each connected client. Returns the ID of the next client to be processed, or NULL when finished.
	 * @note This function should be called in the predicate of a while loop in order to process all clients.
	 * @return The next client ID to process, or 0 if all clients have been processed.
	 */
	unsigned int fetchClient();

	/**
	 * Queries whether there is a client waiting to connect, and accepts if there is.
	 * @note If the maximum number of clients are already connected, this function fails.
	 * @return A valid client ID (greater than 0) on success, or an invalid client ID (0) on failure.
	 */
	unsigned int acceptNewClient();

	/**
	 * Returns the number of active connections. Always in the range [0, maxConnections].
	 * @return The number of active connections.
	 */
	int getNumConnections();

	/**
	 * Terminates the connection for the client with the specified ID.
	 * @param clientID The ID of the client to terminate.
	 */
	void terminateClient(unsigned int clientID);

	/**
	 * Sets which function to use for decoding raw packets. This is a necessary 'hack' in order for the API to
	 * not have to know about custom packet types. The supplied function should implement a Factory pattern that
	 * constructs a new Packet of the appropriate type (and deserializes!).
	 * @param func A pointer to a function to use for decoding custom packet types.
	 */
	void registerPacketDecoder(Packet* (*func)(char*));

private:
	SOCKET listenSocket; ///< Socket to listen for connections on.
	int maxConnections; ///< The maximum number of concurrent connections allowed.
	static unsigned int nextClientID; ///< Keeps track of the ID to assign the next client.
	std::map<unsigned int, SOCKET> sessions; ///< Maps client IDs to SOCKETs.
	char networkBuffer[Packet::MAX_SIZE]; ///< Used for reading data from a socket.
	int totalBytes; ///< Stores the total bytes read into networkBuffer.
	int bytesRead; ///< Keeps track of the number of bytes processed in networkBuffer.
	std::map<unsigned int, SOCKET>::iterator currentClient; ///< Points the client currently being iterated over.
	Packet* (*decodeFunc)(char*); ///< A pointer to the function to use for decoding packets.
};
