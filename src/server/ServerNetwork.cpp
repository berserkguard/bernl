#include "ServerNetwork.h"
#include <iostream>
#include <sstream>

unsigned int ServerNetwork::nextClientID = 1; // An ID of 0 is invalid!

ServerNetwork::ServerNetwork(int port, int maxConnections){
	this->maxConnections = maxConnections;
	this->bytesRead = -1;
	this->currentClient = sessions.end();
	this->decodeFunc = NULL;

	// Initialize Winsock
	WSADATA wsaData;
	if(WSAStartup(MAKEWORD(2, 2), &wsaData)){
		std::cout << "WSAStartup failed. Exiting." << std::endl;
		exit(1);
	}

	// Address info for setting up the listen socket
	addrinfo hints, *result = NULL;

	// Configure hints for requesting TCP socket
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	std::stringstream ss;
	ss << port;
	if(getaddrinfo(NULL, ss.str().c_str(), &hints, &result)){
		std::cout << "getaddrinfo failed (" << WSAGetLastError() << "). Exiting." << std::endl;
		WSACleanup();
		exit(1);
	}

	// Create a SOCKET for connecting to server
	listenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if(listenSocket == INVALID_SOCKET){
		std::cout << "socket failed (" << WSAGetLastError() << "). Exiting." << std::endl;
		freeaddrinfo(result);
		WSACleanup();
		exit(1);
	}

	// Set the mode of the socket to be nonblocking
	unsigned long iMode = 1;
	if(ioctlsocket(listenSocket, FIONBIO, &iMode) == SOCKET_ERROR){
		std::cout << "ioctlsocket failed (" << WSAGetLastError() << "). Exiting." << std::endl;
		freeaddrinfo(result);
		closesocket(listenSocket);
		WSACleanup();
		exit(1);
	}

	// Set up the listening socket
	if(bind(listenSocket, result->ai_addr, (int)result->ai_addrlen) == SOCKET_ERROR){
		std::cout << "bind failed (" << WSAGetLastError() << "). Exiting." << std::endl;
		freeaddrinfo(result);
		closesocket(listenSocket);
		WSACleanup();
		exit(1);
	}
	freeaddrinfo(result);

	// Start listening for new clients attempting to connect
	if(listen(listenSocket, SOMAXCONN) == SOCKET_ERROR){
		std::cout << "listen failed (" << WSAGetLastError() << "). Exiting." << std::endl;
		closesocket(listenSocket);
		WSACleanup();
		exit(1);
	}
}

ServerNetwork::~ServerNetwork(){
	currentClient = sessions.begin();
	while(int curClient = fetchClient()){
		terminateClient(curClient);
	}
	closesocket(listenSocket);
	WSACleanup();
}

void ServerNetwork::registerPacketDecoder(Packet* (*func)(char*)){
	decodeFunc = func;
}

void ServerNetwork::terminateClient(unsigned int clientID){
	auto it = sessions.find(clientID);
	if(it != sessions.end()){
		closesocket(it->second);
		std::cout << "Client terminated: " << it->first << std::endl;
		sessions.erase(it);
	}
}

unsigned int ServerNetwork::acceptNewClient(){
	if(getNumConnections() == maxConnections){
		std::cout << "Error: maximum number of clients connected." << std::endl;
		return 0;
	}

	// If a client is waiting, accept the connection and save the socket
	SOCKET ClientSocket = accept(listenSocket, NULL, NULL);
	if(ClientSocket != INVALID_SOCKET){
		// Disable Nagle on the client's socket to speed up TCP delivery
		char value = 1;
		setsockopt(ClientSocket, IPPROTO_TCP, TCP_NODELAY, &value, sizeof(value));
		sessions.insert(std::pair<unsigned int, SOCKET>(nextClientID, ClientSocket));
		return nextClientID++;
	}
	return 0; // Client ID of 0 is invalid
}

unsigned int ServerNetwork::fetchClient(){
	if(currentClient == sessions.end()){
		currentClient = sessions.begin();
		return 0;
	}
	return (currentClient++)->first;
}

Packet* ServerNetwork::fetchPacket(unsigned int clientID){
	if(bytesRead == -1){
		if(sessions.find(clientID) == sessions.end()){
			std::cout << "fetchPacket: Specified client doesn't exist (" << clientID << ")!" << std::endl;
			return NULL;
		}

		totalBytes = recv(sessions[clientID], networkBuffer, Packet::MAX_SIZE, 0);
		if(totalBytes == 0){
			terminateClient(clientID);
			return NULL;
		}else if(totalBytes < 0){
			return NULL; // Error occurred
		}
		bytesRead = 0;
	}

	if(bytesRead >= totalBytes){
		// Processed all packets in queue, reset
		bytesRead = -1;
		return NULL;
	}

	if(!decodeFunc){
		// A decoding function was never supplied!
		std::cout << "Error, no custom decoding function supplied. See registerPacketDecoder()." << std::endl;
		return NULL;
	}
	Packet* packet = decodeFunc(&networkBuffer[bytesRead]);
	if(!packet){
		std::cout << "Invalid packet received." << std::endl;
		bytesRead = -1;
		return NULL;
	}
	bytesRead += packet->getSize();

	return packet;
}

int ServerNetwork::getNumConnections(){
	return sessions.size();
}

int ServerNetwork::dispatch(unsigned int clientID, Packet& packet){
	if(sessions.find(clientID) == sessions.end()){
		std::cout << "dispatch: Specified client doesn't exist (" << clientID << ")!" << std::endl;
		return -1;
	}
	char* data = packet.serialize();
	int result = send(sessions.at(clientID), data, packet.getSize(), 0);
	delete[] data;
	return result;
}

void ServerNetwork::broadcast(Packet& packet){
	for(auto it = sessions.begin(); it != sessions.end(); ++it){
		if(dispatch(it->first, packet) == SOCKET_ERROR){
			std::cout << "broadcast failed (" << WSAGetLastError() << "). Exiting." << std::endl;
		}
	}
}
