#pragma once

#include <string>
#include <cstring>
#include <cstdlib>

/**
 * @author Ryan Norby
 * @version 1.0
 *
 * @brief Utility class for simplifying object deserialization.
 */
class Deserializer {
public:
	/**
	 * Makes a shallow copy of the buffer.
	 * @param buffer A pointer to a buffer containing serialized data.
	 */
	Deserializer(char* buffer);

	/** Currently does nothing. */
	~Deserializer();

	/**
	 * Templatized method for popping non-pointer data from the beginning of the deserializer buffer.
	 * @note Use pop(char*) if you want to pop an arbitrarily-sized block of memory.
	 * @return A variable of the specified type, initialized according to the buffer.
	 */
	template<typename T>
	T pop(){
		T ret;
		memcpy(reinterpret_cast<char*>(&ret), &buffer[pos], sizeof(T));
		pos += sizeof(T);
		return ret;
	}

private:
	char* buffer; ///< A shallow copy of the buffer specified in the constructor.
	int pos; ///< Keeps track of the current position in the buffer.
};

// Specialized implementation for deserializing std::strings.
template<>
inline std::string Deserializer::pop<std::string>(){
	std::string ret = "";
	ret.resize(strlen(&buffer[pos]));
	strcpy(&ret[0], &buffer[pos]);
	pos += strlen(&buffer[pos]) + 1;
	return ret;
}
