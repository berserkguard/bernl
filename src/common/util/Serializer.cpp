#include "Serializer.h"

Serializer::Serializer(){
	size = 0;
	max = 256;
	buffer = new char[max];
}

Serializer::~Serializer(){
	delete[] buffer;
}

void Serializer::push(char* val, int size){
	checkResize(size);
	memcpy(&buffer[this->size], val, size);
	this->size += size;
}

char* Serializer::getPayload(){
	char* buf = new char[size];
	return (char*)memcpy(buf, buffer, size);
}

void Serializer::checkResize(int size){
	if(this->size + size > max){
		max *= 2;
		buffer = (char*)realloc(buffer, max);
	}
}
