#pragma once

#include <string>
#include <cstring>
#include <cstdlib>

/**
 * @author Ryan Norby
 * @version 1.0
 *
 * @brief Utility class for simplifying object serialization.
 */
class Serializer {
public:
	/** Allocates an internal buffer for writing serialized data to. */
	Serializer();

	/** Frees the internal buffer. */
	~Serializer();

	/**
	 * Templatized method for pushing non-pointer data onto the end of the serializer buffer.
	 * @note Use push(char*, int) if you want to push an arbitrarily-sized block of memory.
	 * @param val The variable to serialize.
	 */
	template<typename T>
	void push(T& val){
		checkResize(sizeof(T));
		memcpy(&buffer[size], reinterpret_cast<const char*>(&val), sizeof(T));
		size += sizeof(T);
	}

	/**
	 * Method for pushing pointer data onto the end of the serializer buffer.
	 * @note You can use the templatized version to push non-pointer data without specifying size explicitly.
	 * @param val A pointer to the data to serialize.
	 * @param size The size of the data to serialize, in bytes.
	 */
	void push(char* val, int size);

	/**
	 * Returns a newly-allocated buffer containing the serialized data. Caller must free memory.
	 * @return A newly-allocated buffer.
	 */
	char* getPayload();

private:
	/**
	 * Helper function for checking whether or not the buffer is about to be overflowed.
	 * If adding 'size' bytes would cause an overflow, resizes the buffer.
	 * @param size The number of bytes to check for overflow condition.
	 */
	void checkResize(int size);

	char* buffer; ///< Used for temporarily storing the serialized data.
	int size; ///< The number of bytes of the buffer currently used.
	int max; ///< The maximum size the buffer can currently hold.
};

// Specialized implementation for serializing std::strings.
template<>
inline void Serializer::push<std::string>(std::string& val){
	checkResize(val.length() + 1);
	memcpy(&buffer[size], val.c_str(), val.length() + 1);
	size += val.length() + 1;
}

