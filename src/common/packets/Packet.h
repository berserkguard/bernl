#pragma once

#include "util/Serializer.h"
#include "util/Deserializer.h"

/**
 * @author Ryan Norby
 * @version 1.0
 *
 * @brief Base class for packets. All packets should inherit from this class.
 */
class Packet {
public:
	/**
	 * Sets the type of the Packet.
	 * @param type The type of the packet.
	 */
	Packet(unsigned int type);

	/** Frees any memory used by the packet. */
	virtual ~Packet();

	/**
	 * Returns an allocated char array containing the packet's serialization.
	 * @return A char array able to be sent over a socket.
	 */
	virtual char* serialize() = 0;

	/**
	 * Modifies the current object to represent the serialized packet.
	 * @param data A serialized packet of the same type.
	 */
	virtual void deserialize(char* data) = 0;

	/**
	 * Returns the size of the (serialized) packet, in bytes.
	 * @return An unsigned int representing the total size of the packet.
	 */
	virtual unsigned int getSize() = 0;

	/**
	 * Returns the type of the packet.
	 * @return The packet's type.
	 */
	unsigned int getType();

	/** The maximum size any one packet is allowed to have. */
	static const int MAX_SIZE = 1024 * 1024;

protected:
	unsigned int type; ///< The type of the packet.
};
