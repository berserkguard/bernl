About this Project
==================

ÜberNL is a lightweight, cross-platform networking library that emphasizes simplicity. As such, it is mostly suited for creating simple networked games/applications. It is intended to be used by beginners who don't have much knowledge about networking or are new to C++. It could also be used in scenarios where time is limited, for example at a hackathon or gamedev competition.

How to Compile
==============

The library has no external dependencies, aside from WinSock2 (Windows) and POSIX sockets (Linux), which are both shipped with the respective OSes.

Development of this library was done in Code::Blocks version 12.11 and compiled with GCC 4.8.1 (Win32 thread model, Dwarf2 exceptions) via MinGW. The Code::Blocks project files are included for both the library and the chat-client example. Because there are no other dependencies, compiling should just be a matter of building from within C::B with no additional configuration necessary. If there is an error building, it is probably due to using an older version of GCC which doesn't support C++11 or using a version that doesn't use the Win32 threading model or the Dwarf2 exception model.

Quick-Start Guide
=================

Common Setup (Server + Client)
------------------------------

### Create Packet Decoder

	// The ServerNetwork/ClientNetwork classes need this function in order to instantiate custom packets.
	Packet* PacketFactory::getPacket(char* data){
		switch(*reinterpret_cast<PacketType*>(data)){
		case PACKET_INIT:
			return new InitPacket(data);
		case PACKET_RESPONSE:
			return new ResponsePacket(data);
		case PACKET_MESSAGE:
			return new MessagePacket(data);
		}
		return (Packet*)NULL;
	}

### Creating a Custom Packet

	// Make sure to include the base class
	#include "packets/Packet.h"

	class CustomPacket : public Packet {
	public:
		CustomPacket() : Packet(PACKET_CUSTOM) {
			customString = "a string";
			customDouble = 3.14159;
			customInt = 1337;
		}
		
		// Implement the abstract methods inherited from base class
		char* serialize(){
			Serializer s;
			s.push(type);
			s.push(customString);
			s.push(customDouble);
			s.push(customInt);
			return s.getPayload();
		}
		void deserialize(char* data){
			Deserializer d(data);
			d.pop<PacketType>();
			customString = d.pop<std::string>();
			customDouble = d.pop<double>();
			customInt = d.pop<int>();
		}
		unsigned int getSize(){
			return (customString.length() + 1) + sizeof(double) + sizeof(int);
		}

	private:
		std::string customString;
		double customDouble;
		int customInt;
	}

Server-side
-----------

### Initial Setup

	// Listen for incoming connections on port 6881 and allow at most 10 concurrent connections
	ServerNetwork* network = new ServerNetwork(6881, 10);

	// Set the function to use for packet decoding (using example from above)
	network->registerPacketDecoder(PacketFactory::getPacket);

### Handling New Connections

	if(unsigned int client = network->acceptNewClient()){
		// New client connected, run custom code here
	}

### Receiving Packets from Clients

	// Iterate over every connected client
	while(int curClient = network->fetchClient()){
		// Process every pending packet for each client
		while(Packet* packet = network->fetchPacket(curClient)){
			switch(packet->getType()){
			case PACKET_INIT:
				// Handle init packet here
				break;
			case PACKET_MESSAGE:
				// Handle message packet here
				break;
			default:
				// Deal with unhandled packet types here
				break;
			}
			delete packet;
		}
	}

### Sending Packets to Client(s)

#### Specific Client

	// Send a response packet to the client with ID '13'
	ResponsePacket packet;
	packet.setMessage("Welcome to the server!");
	network->dispatch(13, packet);

#### All Clients

	// Send a message packet to all clients
	MessagePacket packet;
	packet.setUsername("admin");
	packet.setMessage("Hello World!");
	network->broadcast(packet);

Client-side
-----------

### Initial Setup

	// Connect to the server at IP 127.0.0.1 over port 6881
	ClientNetwork* network = new ClientNetwork("127.0.0.1", 6881);

	// Set the function to use for packet decoding (using example from above)
	network->registerPacketDecoder(PacketFactory::getPacket);

### Sending Packets to Server

	// Send an initial packet to the server
	InitPacket packet;
	packet.setUsername("berserkguard");
	network->dispatch(packet);

### Receiving Packets from Server

	while(Packet* packet = network->fetchPacket()){
		switch(packet->getType()){
		case PACKET_RESPONSE:
			// Handle response packet here
			break;
		case PACKET_MESSAGE:
			// Handle message packet here
			break;
		default:
			// Deal with unhandled packet types here
			break;
		}
		delete packet;
	}
